import axios from "axios";

const newRequest = axios.create({
  baseURL: "https://backend-freelancerflux.onrender.com/api/",
  withCredentials: true,
});

export default newRequest;
